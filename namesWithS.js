function nameWithS(got) {
    // your code goes here
    try {
        const peopelData = got.houses.map(peopleArray => peopleArray.people);
        const namesHaveS = peopelData.reduce((arrayOfs, peopleArray) => {
            peopleArray.map((nameOfS) => {
                if ((nameOfS.name).includes('s') || (nameOfS.name).includes('S')) {
                    arrayOfs.push(nameOfS.name);
                }
            });
            return arrayOfs;
        }, []);
        return (namesHaveS);
    } catch (error) {
        console.error(error);
    }
}

module.exports = nameWithS;