function countAllPeople(got) {
    // your code goes here
    try {
        const totalPeople = got.houses.reduce((countOfPeople, houseData) => {
            countOfPeople += houseData.people.length;
            return countOfPeople;
        }, 0);
        return totalPeople;
    } catch (error) {
        console.error(error);
    }
}

module.exports = countAllPeople;