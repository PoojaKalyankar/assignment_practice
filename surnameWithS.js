function surnameWithS(got) {
    // your code goes here
    try {
        const peopleArray = got.houses.map(houseData => houseData.people);
        const lastNameS = peopleArray.reduce((lastWithS, arrayOfPeople) => {
            arrayOfPeople.forEach(nameLast => {
                let check = nameLast.name.split(' ');
                if (check[check.length - 1].includes('S')) {
                    lastWithS.push(nameLast.name);
                }
            });
            return lastWithS;
        }, []);
        // console.log(lastNameS);
        return lastNameS;
    } catch (error) {
        console.error(error);
    }
}

module.exports = surnameWithS;