function surnameWithA(got) {
    // your code goes here
    try {
        const peopleArray = got.houses.map(houseData => houseData.people);
        const lastNameA = peopleArray.reduce((lastWithA, arrayOfPeople) => {
            arrayOfPeople.forEach(nameLast => {
                let check = nameLast.name.split(' ');
                if (check[check.length - 1].includes('A')) {
                    lastWithA.push(nameLast.name);
                }
            });
            return lastWithA;
        }, []);
        // console.log(lastNameS);
        return lastNameA;
    } catch (error) {
        console.error(error);
    }
}

module.exports = surnameWithA;