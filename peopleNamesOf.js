function peopleNameOfAllHouses(got) {
    // your code goes here
    try {
        const houseData = got.houses.reduce((houseNames, currentHouse) => {
            const arrayOfNames = currentHouse.people.reduce((peopleArray, peopleObjects) => {
                //peopleArray(peopleObjects.name);
                let name = (peopleObjects.name);
                peopleArray.push(name);
                return peopleArray;
            }, []);
            houseNames[currentHouse.name] = arrayOfNames;
            return houseNames;
        }, {});
        const sortedHouse = Object.entries(houseData).sort();
        const finalResult = Object.fromEntries(sortedHouse);
        return finalResult;
    } catch (error) {
        console.error(error);
    }
}

module.exports = peopleNameOfAllHouses;