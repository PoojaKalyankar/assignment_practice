function nameWithA(got) {
    // your code goes here
    try {
        const peopelData = got.houses.map(peopleArray => peopleArray.people);
        const peopleWithA = peopelData.reduce((nameOfA, peopleArray) => {
            peopleArray.forEach(namesHaveA => {
                if (namesHaveA.name.includes('a') || namesHaveA.name.includes('A')) {
                    nameOfA.push(namesHaveA.name);
                }
            })
            return nameOfA;
        }, []);
        //console.log(peopleWithA);
        return peopleWithA;
    } catch (error) {
        console.error(error);
    }
}

module.exports = nameWithA;