function everyone(got) {
    // your code goes here
    try {
        const peopleArray = got.houses.map(peopelName => peopelName.people);
        const peopelName = peopleArray.reduce((names, peopelData) => {
            peopelData.forEach(nameInEach => names.push(nameInEach.name));
            return names;
        }, []);
        return (peopelName);
    } catch (error) {
        console.error(error);
    }
}

module.exports = everyone;