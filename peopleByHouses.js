function peopleByHouses(got) {
  // your code goes here
  try {
    const houseData = got.houses.reduce((houseWithPeople, house) => {
      houseWithPeople[house.name] = house.people.length;
      return houseWithPeople;
    }, {});
    const arrayOfHouse = Object.entries(houseData)
    arrayOfHouse.sort();
    const objecthouse = Object.fromEntries(arrayOfHouse);
    return objecthouse;
  } catch (error) {
    console.error(error);
  }
}

module.exports = peopleByHouses;